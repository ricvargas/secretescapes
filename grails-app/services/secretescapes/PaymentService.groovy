package secretescapes

import grails.gorm.transactions.Transactional

@Transactional
class PaymentService {

    Payment get(Serializable id){
        return Payment.get(id)
    }

    List<Payment> list(Map args){
        return Payment.list(args)
    }

    Long count(){
        return Payment.count()
    }

    Payment save(Payment payment){
        payment.save(failOnError: true)
        payment.accountFrom.balance -= payment.amount
        payment.accountTo.balance += payment.amount

        payment.accountFrom.save(failOnError: true)
        payment.accountTo.save(failOnError: true)

        return payment
    }

    /**
     * This is an example using HQL queries.
     */
    List<Payment> listAccountPayments(Account account, Map args){
        List payments = Payment.executeQuery("from Payment where accountFrom.id = :account or accountTo.id = :account", [account: account?.id], args)
        return payments
    }

    /**
     * This is an example using Dynamic Methods
     */
    Long countAccountPayments(Account account){
        return Payment.countByAccountFromOrAccountTo(account, account)
    }
}
