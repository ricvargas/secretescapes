package secretescapes

class BootStrap {

    PaymentService paymentService

    def init = { servletContext ->

        Account a1 = new Account(holderName: "Peter Parker", emailAddress: "spidey@email.com").save(failOnError: true)
        Account a2 = new Account(holderName: "Tony Stark", emailAddress: "tony@starkenterprises.com").save(failOnError: true)
        Account a3 = new Account(holderName: "Natalia Romanova", emailAddress: "widow@shield.com").save(failOnError: true)
        Account a4 = new Account(holderName: "Pepper Potts", emailAddress: "pepper.potts@starkenterprises.com").save(failOnError: true)

        paymentService.save(new Payment(accountFrom: a1, accountTo: a2, amount: 150D))
        paymentService.save(new Payment(accountFrom: a1, accountTo: a3, amount: 50D))
        paymentService.save(new Payment(accountFrom: a4, accountTo: a1, amount: 150D))
        paymentService.save(new Payment(accountFrom: a3, accountTo: a1, amount: 150D))
    }
    def destroy = {
    }
}
