package secretescapes

import grails.testing.mixin.integration.Integration
import grails.gorm.transactions.Rollback
import spock.lang.Specification
import org.hibernate.SessionFactory

@Integration
@Rollback
class AccountServiceSpec extends Specification {

    AccountService accountService
    SessionFactory sessionFactory

    private void setupData() {
        if(!Account.count()){
            new Account(holderName: "Peter Parker", emailAddress: "spidey@email.com").save(failOnError: true)
            new Account(holderName: "Tony Stark", emailAddress: "tony@starkenterprises.com").save(failOnError: true)
            new Account(holderName: "Natalia Romanova", emailAddress: "widow@shield.com").save(failOnError: true)
            new Account(holderName: "Pepper Potts", emailAddress: "pepper.potts@starkenterprises.com").save(failOnError: true)
        }
    }

    void "test get"() {
        setupData()

        expect:
        accountService.get(1) != null
    }

    void "test list"() {
        setupData()

        when:
        List<Account> accountList = accountService.list(max: 4, offset: 0)

        then:
        assert accountList.size() == 4
    }

    void "test count"() {
        setupData()

        expect:
        accountService.count() == 4
    }

}
