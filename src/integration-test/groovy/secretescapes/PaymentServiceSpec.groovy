package secretescapes

import grails.testing.mixin.integration.Integration
import grails.gorm.transactions.Rollback
import spock.lang.Specification
import org.hibernate.SessionFactory

@Integration
@Rollback
class PaymentServiceSpec extends Specification {

    PaymentService paymentService
    SessionFactory sessionFactory

    private void setupData() {
        if(!Account.count()){
            Account a1 = new Account(holderName: "Peter Parker", emailAddress: "spidey@email.com").save(failOnError: true)
            Account a2 = new Account(holderName: "Tony Stark", emailAddress: "tony@starkenterprises.com").save(failOnError: true)
            Account a3 = new Account(holderName: "Natalia Romanova", emailAddress: "widow@shield.com").save(failOnError: true)
            Account a4 = new Account(holderName: "Pepper Potts", emailAddress: "pepper.potts@starkenterprises.com").save(failOnError: true)

            paymentService.save(new Payment(accountFrom: a1, accountTo: a2, amount: 150D))
            paymentService.save(new Payment(accountFrom: a1, accountTo: a3, amount: 50D))
            paymentService.save(new Payment(accountFrom: a4, accountTo: a1, amount: 150D))
            paymentService.save(new Payment(accountFrom: a3, accountTo: a1, amount: 150D))
        }
    }

    void "test get"() {
        setupData()

        expect:
        paymentService.get(1) != null
    }

    void "test list"() {
        setupData()

        when:
        List<Payment> paymentList = paymentService.list(max: 4, offset: 2)

        then:
        assert paymentList.size() == 2
    }

    void "test count"() {
        setupData()

        expect:
        paymentService.count() == 4
    }

    void "test save"() {
        given: "A Payment is saved with the right data"
        Account a1 = Account.get(1)
        Double oldBalance1 = a1.balance

        Account a2 = Account.get(2)
        Double oldBalance2 = a2.balance

        Payment payment = new Payment(accountFrom: a1, accountTo: a2, amount: 100D)
        paymentService.save(payment)

        expect: "The Payment is saved and the accountFrom and accountTo balances are updated acordingly"
        assert payment.id != null
        assert payment.accountFrom.balance == oldBalance1 - payment.amount
        assert payment.accountTo.balance == oldBalance2 + payment.amount

    }

    void "test save with incorrect data"() {
        when:
        Account a1 = Account.get(1)
        Payment payment = new Payment(accountFrom: a1, accountTo: a1, amount: 100D)
        payment.validate()

        then:
        assert payment.errors
    }
}
