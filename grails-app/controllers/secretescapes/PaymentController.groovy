package secretescapes

import grails.validation.ValidationException
import static org.springframework.http.HttpStatus.*
import grails.plugins.mail.MailService

class PaymentController {

    PaymentService paymentService
    MailService mailService

    static allowedMethods = [save: "POST", update: "PUT", delete: "DELETE"]

    def index(Integer max) {
        params.max = Math.min(max ?: 10, 100)
        respond paymentService.list(params), model:[paymentCount: paymentService.count()]
    }

    def create() {
        respond new Payment(params),
            view:'pay', model:[accounts:Account.list(), accountFrom: Account.get(1), accountTo: Account.get(2)]
    }

    def save(Payment payment) {
        if (payment == null) {
            notFound()
            return
        }

        try {
            paymentService.save(payment)

            mailService.sendMail {
               to payment?.accountFrom?.emailAddress,payment?.accountTo?.emailAddress
               from "transfer@secretescapes.com"
               subject "Test transfer successful! (This is a test)"
               html "<h1>Transfer successful!</h1><b>From: </b>${payment?.accountFrom?.holderName}<br /><b>To: </b>${payment?.accountTo?.holderName}<br><b>Amount:</b> ${payment?.amount}"
            }

        } catch (ValidationException e) {
            respond payment.errors, view:'pay',
                model:[accounts:Account.list(), accountFrom: payment?.accountFrom, accountTo: payment?.accountTo]
            return
        }

        request.withFormat {
            form multipartForm {
                flash.message = "Transfer Successful"
                redirect controller:'account', action:'index', method:"GET"
            }
            '*' { respond payment, [status: CREATED] }
        }
    }

    protected void notFound() {
        request.withFormat {
            form multipartForm {
                flash.message = message(code: 'default.not.found.message', args: [message(code: 'payment.label', default: 'Payment'), params.id])
                redirect action: "index", method: "GET"
            }
            '*'{ render status: NOT_FOUND }
        }
    }
}
