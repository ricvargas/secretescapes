package secretescapes

import grails.testing.gorm.DomainUnitTest
import spock.lang.Specification

class PaymentSpec extends Specification implements DomainUnitTest<Payment> {

    Account a1, a2

    def setup() {
        a1 = new Account(holderName: "Tony Stark", emailAddress: "tony@starkenterprises.com").save(failOnError: true)
        a2 = new Account(holderName: "Pepper Potts", emailAddress: "pepper.potts@starkenterprises.com").save(failOnError: true)
    }

    def cleanup() {
        a1.delete()
        a2.delete()
    }

    void "test create a new Payment withwithout the required parameters is invalid"(){
        given: "a new Payment is created with an amount higher than the accountFrom balance"
            Payment payment = new Payment()

        expect: "the Payment is invalid"
            !payment.validate()
    }

    void "test create a new Payment with an overdraft is invalid"(){
        given: "a new Payment is created with an amount higher than the accountFrom balance"
            Payment payment = new Payment(accountFrom: a1, accountTo: a2, amount: 4000)

        expect: "the Payment is invalid"
            !payment.validate()
    }

    void "test create a new Payment with the same accountFrom and accountTo"(){
        given: "a new Payment is created with the same account as the accountFrom and the accountTo"
            Payment payment = new Payment(accountFrom: a1, accountTo: a1, amount: 100)

        expect: "the Payment is invalid"
            !payment.validate()
    }

    void "test create a new Payment with the right data"(){
        given: "a new Payment is created with the right data"
            Payment payment = new Payment(accountFrom: a1, accountTo: a2, amount: 100)

        expect: "the Payment is valid"
            payment.validate()
    }

}
