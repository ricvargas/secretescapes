package secretescapes

import groovy.transform.EqualsAndHashCode

@EqualsAndHashCode(includes='holderName, emailAddress')
class Account {

    String holderName
    Double balance = 200D
    String emailAddress

    static constraints = {
        emailAddress email:true
        balance min: 0D
    }

}
