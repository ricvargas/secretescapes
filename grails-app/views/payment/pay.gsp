<%@ page import="secretescapes.Account" %>

<!DOCTYPE html>
<html>
    <head>
        <meta name="layout" content="main" />
        <title><%@ page import="secretescapes.Account" %></title>
    </head>
    <body>
        <div class="nav row" role="navigation">
            <div class="col-md">
                <a href="#create-payment" class="skip" tabindex="-1"><g:message code="default.link.skip.label" default="Skip to content&hellip;"/></a>
                <ul>
                    <li><a class="home" href="${createLink(uri: '/')}"><g:message code="default.home.label"/></a></li>
                </ul>
            </div>
        </div>

        <g:if test="${flash.message}">
            <div class="message" role="status">${flash.message}</div>
        </g:if>
        <g:hasErrors bean="${this.payment}">
        <ul class="errors" role="alert">
            <g:eachError bean="${this.payment}" var="error">
            <li <g:if test="${error in org.springframework.validation.FieldError}">data-field-id="${error.field}"</g:if>><g:message error="${error}"/></li>
            </g:eachError>
        </ul>
        </g:hasErrors>

        <div id="create-payment" class="content scaffold-create row" role="main">
            <div class="col-md">
                <h2>Pay</h2>
                <g:form resource="${this.payment}" method="POST" class="needs-validation">
                    <fieldset class="form">
                        <div class="form-group row">
                            <label class="col-sm-2 col-form-label">Account From:</label>
                            <div class="col-sm-4">
                                <g:select name="accountFrom" class="form-control" from="${this.accounts}" optionKey="id" optionValue="holderName"
                                    value="${this.accountFrom?.id}"/>
                                <div class="invalid-feedback">Must be different</div>
                            </div>
                        </div>

                        <div class="form-group row">
                            <label class="col-sm-2 col-form-label">Account To:</label>
                            <div class="col-sm-4">
                                <g:select name="accountTo" class="form-control" from="${this.accounts}" optionKey="id" optionValue="holderName"
                                    value="${this.accountTo?.id}" />
                                <div class="invalid-feedback">Must be different</div>
                            </div>
                        </div>

                        <div class="form-group row">
                            <label class="col-sm-2 col-form-label">Amount:</label>
                            <div class="col-sm-4">
                                <g:field type="number" name="amount" class="form-control" required="true" value="${this.payment.amount}"  />
                            </div>
                        </div>
                    </fieldset>
                    <fieldset class="buttons">
                        <g:submitButton name="create" class="save" value="${message(code: 'default.button.create.label', default: 'Create')}" />
                        <g:link class="delete" url="/" resource="${this.account}"><g:message code="default.cancel.label" default="Cancel" /></g:link>
                    </fieldset>
                </g:form>
            </div>
        </div>

        <script>
            $(document).ready(function(){
                $("select").change(function(){
                    let accountFrom = $("#accountFrom")
                    let accountTo = $("#accountTo")

                    if(accountFrom.val() == accountTo.val()){
                        accountFrom.addClass("is-invalid")
                        accountTo.addClass("is-invalid")
                    }else{
                        accountFrom.removeClass("is-invalid")
                        accountTo.removeClass("is-invalid")
                    }
                })
            });
        </script>

    </body>
</html>
