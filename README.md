# Instructions to test and run this program.
### Requirements
- You must have __Grails 4.X__ installed on your machine.
- If you are running the application, you must have configure the application with valid email credentials to send emails. This is not required if you only need to test the application.
### Configure a valid email account
- Make sure you set up rights to send emails from your email provider through third party applications, otherwise the application will show an error when trying to send emails.
- Go to the file __application.groovy__ and edit the configuration with the right Email provider and credentials, for example:
#####application.groovy (Google-based account)
		grails {
			mail{
				host = "smtp.gmail.com"
				port = 465
				username = "email@gmail.com"
				password = "password"
				props = ["mail.smtp.auth":"true",
					 "mail.smtp.socketFactory.port":"465",
					 "mail.smtp.socketFactory.class":"javax.net.ssl.SSLSocketFactory",
					 "mail.smtp.socketFactory.fallback":"false"]
			}
		}
#####application.groovy (Outlook-based account)
		grails {
			mail{
				host = "smtp.office365.com"
				port = 587
				username = "email@email.com"
				password = "Password"
				 props = ["mail.smtp.starttls.enable":"true", "mail.smtp.port":"587"]
			}
		}
### How to run the application
- Open a command line and run: grails run-app
- You can edit the __Bootstrap.groovy__ file as you like to set up receiving email accounts.
### How to test the application
- Open a coommand line and run: grails test-app
### If you have any questions or comments
- Please let me know at ricvargas@gmail.com
THANKS !!
