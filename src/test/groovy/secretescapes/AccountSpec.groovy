package secretescapes

import grails.testing.gorm.DomainUnitTest
import spock.lang.Specification

class AccountSpec extends Specification implements DomainUnitTest<Account> {

    void "test create an invalid Account"() {
        given: "a new Account is created without the required parameters"
            Account account = new Account()

        expect: "the Account is invalid"
            !account.validate()
    }

    void "test create an Account with a negative balance"(){
        given: "a new Account is created with a negative balance"
            Account account = new Account(holderName: "Peter Parker", balance: -1)

        expect: "the Account is invalid"
            !account.validate()
    }

    void "test create an Account with the right data"(){
        given: "a new Account is created the right data"
            Account account = new Account(holderName: "Peter Parker", balance: 500, emailAddress: "spidey@email.com")

        expect: "the Account is valid"
            account.validate()
    }

}
