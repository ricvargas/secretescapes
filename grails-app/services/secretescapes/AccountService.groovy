package secretescapes

import grails.gorm.services.Service

@Service(Account)
interface AccountService {

    Account get(Serializable id)

    List<Account> list(Map args)

    Long count()

}
