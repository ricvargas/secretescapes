package secretescapes

class Payment {
    Account accountFrom
    Account accountTo
    Double amount

    // static belongsTo = [Account]

    static constraints = {
        amount validator: { val, obj ->
            if(val > obj.accountFrom?.balance){
                return ['overdraft']
            }
        }

        accountTo validator: { val, obj, errors ->
            if (obj.accountFrom == val) errors.rejectValue('accountTo', 'equals')
        }
    }

}
