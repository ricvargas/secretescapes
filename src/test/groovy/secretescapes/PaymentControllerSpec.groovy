package secretescapes

import grails.testing.gorm.DomainUnitTest
import grails.testing.web.controllers.ControllerUnitTest
import grails.validation.ValidationException
import spock.lang.*
import grails.plugins.mail.MailService

class PaymentControllerSpec extends Specification implements ControllerUnitTest<PaymentController>, DomainUnitTest<Payment> {



    void "Test the index action returns the correct model"() {
        given:
        controller.paymentService = Mock(PaymentService) {
            1 * list(_) >> []
            1 * count() >> 0
        }

        when:"The index action is executed"
        controller.index()

        then:"The model is correct"
        !model.paymentList
        model.paymentCount == 0
    }

    void "Test the create action returns the correct model"() {
        when:"The create action is executed"
        controller.create()

        then:"The model is correctly created"
        model.payment!= null
    }

    void "Test the save action with a null instance"() {
        when:"Save is called for a domain instance that doesn't exist"
        request.contentType = FORM_CONTENT_TYPE
        request.method = 'POST'
        controller.save(null)

        then:"A 404 error is returned"
        response.redirectedUrl == '/payment/index'
        flash.message != null
    }

    void "Test the save action correctly persists"() {
        given:
        controller.paymentService = Mock(PaymentService) {
            1 * save(_ as Payment)
        }
        controller.mailService = Mock(MailService) {
            1 * sendMail {_}
        }

        when:"The save action is executed the Payment is saved and the email is sent"
        response.reset()
        request.contentType = FORM_CONTENT_TYPE
        request.method = 'POST'

        def params = [
            accountFrom: new Account(holderName: "Peter Parker", emailAddress: "spidey@email.com"),
            accountTo: new Account(holderName: "Tony Stark", emailAddress: "tony@starkenterprises.com"),
            amount: 100D
        ]

        def payment = new Payment(params)
        payment.id = 1

        controller.save(payment)

        then:"A redirect is issued to the show action"
        response.redirectedUrl == '/'
        controller.flash.message != null
    }

    void "Test the save action with an invalid instance"() {
        given:
        controller.paymentService = Mock(PaymentService) {
            1 * save(_ as Payment) >> { Payment payment ->
                throw new ValidationException("Invalid instance", payment.errors)
            }
        }

        controller.mailService = Mock(MailService) {
            0 * sendMail {_}
        }

        when:"The save action is executed with an invalid instance no record is saved an no email is sent"
        request.contentType = FORM_CONTENT_TYPE
        request.method = 'POST'
        def payment = new Payment()
        controller.save(payment)

        then:"The pay view is rendered again with the correct model"
        model.payment != null
        view == 'pay'
    }

}
