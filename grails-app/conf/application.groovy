/** === MAIL CONFIGURATION === **/
// Google Based
grails {
	mail{
		host = "smtp.gmail.com"
		port = 465
		username = "test@gmail.com"
		password = "password"
		props = ["mail.smtp.auth":"true",
				 "mail.smtp.socketFactory.port":"465",
				 "mail.smtp.socketFactory.class":"javax.net.ssl.SSLSocketFactory",
				 "mail.smtp.socketFactory.fallback":"false"]
	}
}

// Microsoft / Outlook Based
// grails {
// 	mail{
// 		host = "smtp.office365.com"
// 		port = 587
// 		username = "email@email.com"
// 		password = "Password"
// 		 props = ["mail.smtp.starttls.enable":"true", "mail.smtp.port":"587"]
// 	}
// }
