<!DOCTYPE html>
<html>
    <head>
        <meta name="layout" content="main" />
        <g:set var="entityName" value="${message(code: 'account.label', default: 'Account')}" />
        <title><g:message code="default.list.label" args="[entityName]" /></title>
    </head>
    <body>
        <a href="#list-account" class="skip" tabindex="-1"><g:message code="default.link.skip.label" default="Skip to content&hellip;"/></a>
        <div class="nav row" role="navigation">
            <div class="col-md">
                <ul>
                    <li><a class="home" href="${createLink(uri: '/')}"><g:message code="default.home.label"/></a></li>
                    <li><g:link class="create" controller="payment" action="create">Make a Payment</g:link></li>
                </ul>
            </div>
        </div>
        <div id="list-account" class="content scaffold-list row" role="main">
            <div class="col-md">
                <h1><g:message code="default.list.label" args="[entityName]" /></h1>
                <g:if test="${flash.message}">
                    <div class="message" role="status">${flash.message}</div>
                </g:if>
                <table>
                    <thead>
                        <th>Name</th>
                        <th>Balance</th>
                    </thead>
                    <tbody>
                        <g:each in="${accountList}" var="account">
                            <tr>
                                <td><g:link action="show" id="${account?.id}">${account?.holderName}</g:link></td>
                                <td>&pound; ${account?.balance}</td>
                            </tr>
                        </g:each>
                    </tbody>
                </table>

                <div class="pagination">
                    <g:paginate total="${accountCount ?: 0}" />
                </div>
            </div>
        </div>
    </body>
</html>
