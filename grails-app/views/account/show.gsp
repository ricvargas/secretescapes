<!DOCTYPE html>
<html>
    <head>
        <meta name="layout" content="main" />
        <g:set var="entityName" value="${message(code: 'account.label', default: 'Account')}" />
        <title><g:message code="default.show.label" args="[entityName]" /></title>
    </head>
    <body>
        <a href="#show-account" class="skip" tabindex="-1"><g:message code="default.link.skip.label" default="Skip to content&hellip;"/></a>
        <div class="nav row" role="navigation">
            <div class="col-md">
                <ul>
                    <li><a class="home" href="${createLink(uri: '/')}"><g:message code="default.home.label"/></a></li>
                    <li><g:link class="list" action="index"><g:message code="default.list.label" args="[entityName]" /></g:link></li>
                </ul>
            </div>
        </div>
        <div id="show-account" class="content scaffold-show row" role="main">
            <div class="col-md">
                <h1><g:message code="default.show.label" args="[entityName]" /></h1>
                <g:if test="${flash.message}">
                    <div class="message" role="status">${flash.message}</div>
                </g:if>

                <div class="row">
                    <div class ="col-md">
                        <p><b>Holder name:</b> ${this.account?.holderName}</p>
                        <p><b>Balance:</b> &pound;${this.account?.balance}</p>
                        <p><b>Email:</b> ${this.account?.emailAddress}</p>
                    </div>
                </div>
                <div class="row">
                    <div class ="col-md">
                        <table>
                            <thead>
                                <th>Name</th>
                                <th>Amount</th>
                            </thead>
                            <tbody>
                                <g:each in="${this.payments}" var="payment">
                                    <tr>
                                        <g:if test="${payment?.accountFrom == this.account}">
                                            <td>${payment?.accountTo?.holderName}</td>
                                            <td class="outgoingPayment">- &pound;${payment?.amount}</td>
                                        </g:if>
                                        <g:else>
                                            <td>${payment?.accountFrom?.holderName}</td>
                                            <td class="incomingPayment">+ &pound;${payment?.amount}</td>
                                        </g:else>
                                    </tr>
                                </g:each>
                            </tbody>
                            <tfooter>
                                <tr>
                                    <th><b>Total</b></th>
                                    <th>&pound;${this.account?.balance}</th>
                                </tr>
                            </tfooter>
                        </table>
                        <div class="pagination">
                            <g:paginate total="${this.paymentsCount ?: 0}" />
                        </div>

                    </div>
                </div>

            </div>
        </div>
    </body>
</html>
