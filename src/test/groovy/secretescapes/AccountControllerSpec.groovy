package secretescapes

import grails.testing.gorm.DomainUnitTest
import grails.testing.web.controllers.ControllerUnitTest
import grails.validation.ValidationException
import spock.lang.*

class AccountControllerSpec extends Specification implements ControllerUnitTest<AccountController>, DomainUnitTest<Account> {

    // This is required because I'm testing multiple domains: Account and Payment.
    Class<?>[] getDomainClassesToMock(){
        return [Account, Payment] as Class[]
    }

    Account a1, a2
    Payment p

    def setup() {
        a1 = new Account(holderName: "Peter Parker", emailAddress: "spidey@email.com").save(failOnError: true)
        a2 = new Account(holderName: "Tony Stark", emailAddress: "tony@starkenterprises.com").save(failOnError: true)
        p = new Payment(accountFrom: a1, accountTo: a2, amount: 150D).save(failOnError: true)
    }

    def cleanup() {
        a1.delete()
        a2.delete()
        p.delete()
    }

    void "Test the index action returns the list of Accounts"() {
        given:
        controller.accountService = Mock(AccountService) {
            1 * list(_) >> [a1, a2]
            1 * count() >> 2
        }

        when:"The index action is executed"
        controller.index()

        then:"The list of accounts is returned"
        model.accountList
        model.accountCount == 2
    }

    void "Test the show action with a null id"() {
        given:
        controller.accountService = Mock(AccountService) {
            1 * get(null) >> null
        }
        controller.paymentService = Mock(PaymentService) {
            1 * listAccountPayments(_,_) >> null
            1 * countAccountPayments(_) >> 0
        }

        when:"The show action is executed with a null domain"
        controller.show(null)

        then:"A 404 error is returned"
        response.status == 404
    }

    void "Test the show action with a valid id"() {
        given:
        List<Payment> payments = [p]
        controller.accountService = Mock(AccountService) {
            1 * get(1) >> a1
        }
        controller.paymentService = Mock(PaymentService) {
            1 * listAccountPayments(_,_) >> payments
            1 * countAccountPayments(_) >> 1
        }

        when: "An Account ID is passed to the show action"
        controller.show(1)

        then: "A model is populated with the Account and the list of Payments"
        model.account instanceof Account
        model.payments
        model.payments.size() == 1
        model.payments.find { it.amount == 150D }
        model.paymentsCount == 1
    }
}
