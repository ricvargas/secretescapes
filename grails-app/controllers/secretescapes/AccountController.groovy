package secretescapes

import grails.validation.ValidationException
import static org.springframework.http.HttpStatus.*

class AccountController {

    AccountService accountService
    PaymentService paymentService

    static allowedMethods = [save: "POST", update: "PUT"]

    def index(Integer max) {
        params.max = Math.min(max ?: 10, 100)
        respond accountService.list(params), model:[accountCount: accountService.count()]
    }

    def show(Long id) {
        Account account = accountService.get(id)
        Collection payments = paymentService.listAccountPayments(account, params)
        Long paymentsCount = paymentService.countAccountPayments(account)

        respond account, model:[payments:payments, paymentsCount: paymentsCount]
    }

}
